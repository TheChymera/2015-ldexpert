A compiled PDF of the latest major revision can be downloaded [here](http://chymera.eu/pres/ld_expert/pres.pdf).

A video of a presentation held with [an older revision](https://bitbucket.org/TheChymera/ld_expert/src/5688ebb7b6f2?at=master) of these slides, can be seen [here](https://www.youtube.com/watch?v=SdJ31Ggpr54).

# “Free and Open Source Sientific Software”
Slides for the ETHZ/UZH “Linux Days” Expert Course - organized by [project21] TheAlternative.

###This document presents:

* a brief overview of Free and Open Source Software (FOSS) in science
* a run-down of important neuroscience packages and package collections
* a minimal neuroimaging python program example
* a minimal package atom for Portage
* a PythonTeX document using the aforementioned program to produce “live” figures

###License: [Creative Commons Attribution-ShareAlike 3.0 Unported](http://creativecommons.org/licenses/by-sa/3.0/)
